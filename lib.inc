section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 
 
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rsi, rsi

    .loop:
        mov al, [rdi + rsi]

        test al, al
        je .end

        inc rsi

        jmp .loop
    .end:
        mov rax, rsi
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdi, 1
    mov rdx, rax
    mov rax, 1

    syscall

    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi

    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1

    syscall

    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r9, rsp
    mov rsi, 10
    dec rsp
    mov byte[rsp], 0

    mov rax, rdi

    .loop:
        xor rdx, rdx
        div rsi

        dec rsp
        add rdx, '0' ;GET ASCII code of number
        mov [rsp], dl

        test rax, rax
        jnz .loop

    mov rdi, rsp
    push r9
    call print_string
    pop rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .unsigned

    push rdi

    mov rdi, '-'
    call print_char
    
    pop rdi

    neg rdi

    .unsigned:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r10, r10 ;counter
    .loop:
        mov al, [rdi + r10]
        cmp al, [rsi + r10] 
        jne .false 
        test rax, rax
        je .true
        inc r10 
        jmp .loop
    .false:
        xor rax, rax
        ret
    .true:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push rax
    mov rsi, rsp

    syscall

    pop rax

    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi

    .skip_first_symbols:
        call read_char

        cmp rax, ' '
        je .skip_first_symbols

        cmp rax, `\n`
        je .skip_first_symbols

        cmp rax, `\t`
        je .skip_first_symbols

    xor r10, r10 ;counter

    .loop:
        cmp rax, `\t`
        je .end

        cmp rax, `\n`
        je .end

        cmp rax, ' '
        je .end

        test rax, rax
        je .end

        cmp r13, r10
        je .out_of_buffer

        mov byte[r12 + r10], al

        inc r10
        
        push r10
        call read_char
        pop r10

        jmp .loop

    .end:
        xor al, al
        mov byte[r12 + r10], al
        mov rax, r12
        mov rdx, r10
        pop r13
        pop r12
        ret
    .out_of_buffer:
        xor rax, rax
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r10, r10 ;counter
    xor rdx, rdx
    xor rax, rax
    mov r8, 10
    .loop:
        mov dl, byte[rdi + r10]
        cmp rdx, '0'
        jl .end
        cmp rdx, '9'
        jg .end
        sub rdx, '0'
        push rdx 
        mul r8 ;this command reset rdx
        pop rdx
        add rax, rdx
        inc r10
        jmp .loop
    .end:
        mov rdx, r10
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jnz .unsigned
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
    .unsigned:
        jmp parse_uint

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rsi
    push rdi
    call string_length
    pop rdi
    pop rsi
    xor r10, r10 ;counter
    cmp rax, rdx
    jge .out_of_buffer
    .loop:
        mov al, byte[rdi + r10]
        mov byte[rsi + r10], al
        inc r10
        test al, al
        jne .loop
        mov rax, r10
        ret

    .out_of_buffer:
        xor rax, rax
        ret
